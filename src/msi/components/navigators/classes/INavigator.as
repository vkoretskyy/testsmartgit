package msi.components.navigators.classes
{
	import msi.components.navigators.layouts.INavigatorLayout;
	
	import mx.core.IDeferredContentOwner;
	import mx.core.ISelectableList;
	import mx.core.IVisualElement;
	import mx.core.IVisualElementContainer;
	
	import spark.components.supportClasses.GroupBase;
	
	
	public interface INavigator extends ISelectableList, IVisualElement
	{
		
		function get layout():msi.components.navigators.layouts.INavigatorLayout;
		function set layout( value:INavigatorLayout ):void
	}
}